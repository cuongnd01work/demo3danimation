﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private float offsetY;
    [SerializeField] private float offsetZ;
    private void Update()
    {
        transform.position = new Vector3(player.position.x,
      offsetY + player.position.y, player.position.z + offsetZ);
    }
}
