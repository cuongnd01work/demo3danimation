﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Models : MonoBehaviour
{

    public GameObject ModeDummy => modeDummy;
    public GameObject ModeUnityChan => modeUnityChan;


    [SerializeField] private GameObject modeDummy;
    [SerializeField] private GameObject modeUnityChan;
}
