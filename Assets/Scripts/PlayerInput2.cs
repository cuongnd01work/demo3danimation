﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class PlayerInput2 : MonoBehaviour
{
    private Animator _animator;
    private float _buffMove = 3.5f;
    private float _speed;
    private float _horizontal;
    private float _vertical;
    private Rigidbody _rigidbody3D;
    public bool StopAll = false;
    [SerializeField] private bool _jumping = true;
    [SerializeField] private GameObject _model;
    private Models _models;
    private Vector3 _movementVector;
    private float _jumpPower = 2.5f;

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _rigidbody3D = GetComponent<Rigidbody>();
        _models = GetComponent<Models>();
    }
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.U))
        {
            ChangeModel(_models.ModeUnityChan);
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            ChangeModel(_models.ModeDummy);
        }
        if (!StopAll && !_jumping)
        {
            _horizontal = Input.GetAxis("Horizontal");
            _vertical = Input.GetAxis("Vertical");
            if (Input.GetKey(KeyCode.LeftShift))
            {
                _speed = 1.5f;
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                _speed = 0.5f;
            }
            else if (_vertical != 0 || _horizontal != 0)
            {
                _speed = 1;
            }
            else
            {
                _speed = 0;
            }

            _animator.SetFloat("horizontal", _horizontal);
            _animator.SetFloat("vertical", _vertical);
            _animator.SetFloat("speed", _speed);
            _movementVector = new Vector3(_horizontal, 0, _vertical);
            _rigidbody3D.velocity = _movementVector * _speed * _buffMove;
            if (Input.GetKey(KeyCode.Space))
            {
                _jumping = true;
                _animator.Play("Jump");
                _movementVector.y = _jumpPower;
                _rigidbody3D.velocity = _movementVector * 2;
            }
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        _jumping = false;
    }
    private void OnCollisionExit(Collision collision)
    {
        _jumping = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Enemy"))
        {
            StopAll = true;
            _animator.Play("Hit");
        }
        if (collision.gameObject.tag.Equals("Boss"))
        {
            StopAll = true;
            _animator.Play("Dead");
        }
    }
    public void ChangeModel(GameObject model)
    {
        Destroy(_model);
        _model = Instantiate(model, transform.position, Quaternion.identity, transform);
        _animator = _model.GetComponent<Animator>();
    }
}
